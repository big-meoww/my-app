import { View, Swiper, SwiperItem, Image } from '@tarojs/components';
import { useLoad } from '@tarojs/taro';
import { background }  from '@/utils/data.js';
import './index.scss';

export default function Index() {
  useLoad(() => {
    console.log('Page loaded.');
  });
  const swiperOption = {
    circular: true,
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
  };

  return (
    <View className='Swiper'>
      <Swiper
        className='_swiper'
        indicator-dots={swiperOption.indicatorDots}
        autoplay={swiperOption.autoplay}
        interval={swiperOption.interval}
        duration={swiperOption.duration}
        circular={swiperOption.circular}
      >
        {background &&
          background.map((item, index) => (
            <SwiperItem key={index}>
              <view className='swiper-item _swiper'>
                <Image
                  src={item.src}
                  style='width: 100%;height: 100%;background: #fff;'
                />
              </view>
            </SwiperItem>
          ))}
      </Swiper>
    </View>
  );
}
