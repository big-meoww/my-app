export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/orderFood/index',
    'pages/about/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#d1322f',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'white'
  },
  tabBar: {
    position: 'bottom',
    color: '#e58229',
    selectedColor: '#d1322f',
    backgroundColor: '#fff',
    list: [
      { pagePath: 'pages/index/index', text: '首页', iconPath: './assets/img/home_02.png', selectedIconPath: './assets/img/home_1.png' },
      { pagePath: 'pages/about/index', text: '关于', iconPath: './assets/img/about_02.png', selectedIconPath: './assets/img/about_1.png' }
    ]
  }
})
