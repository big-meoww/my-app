import { useState } from 'react';
import { View, Text, ScrollView } from '@tarojs/components';
import Taro, { useLoad } from '@tarojs/taro';
import { classData, dishesData } from './data';
import './index.scss';

export default function MenuClassification() {

  let load = true
  let [activeIndex, setActiveIndex] = useState(0);
  let [uesDishesData, setUesDishesData] = useState(dishesData);
  let [verticalNavTop, setVerticalNavTop] = useState(0);
  let [mainCur, setMainCur] = useState('0')
  useLoad(() => {
    console.log('Page loaded.');
  });

  const setdishesClick = (item, i) => {
    setActiveIndex(i);
    setVerticalNavTop((i - 1) * 114);
    setMainCur(i)
  };
  const scrollStyle = {
    height: '70vh',
  };
  const onScroll = (e) => {
    let tabHeight = 0;
    if (load) {
      let newUesDishesData = uesDishesData
      newUesDishesData.map(v => {
        v.map((item: any, i) => {
          Taro.createSelectorQuery().select("#main-" + i).fields({
            size: true,
          },function (res) {
            item.top = tabHeight
            tabHeight = tabHeight + res.height
            item.bottom = tabHeight
          }).exec()
        })
      })
      setTimeout(() => {
        console.log(newUesDishesData, 'newUesDishesDatanewUesDishesData');
        
      })
      setUesDishesData(newUesDishesData)
      load = false
    } else {
      let scrollTop = e.detail.scrollTop;
      for (let j = 0; j < uesDishesData.length; j++) {
        let item:any = uesDishesData[j]
        for (let i = 0; i < item.length; i++) {
          if (scrollTop > item[i].top && scrollTop < item[i].bottom) {
            verticalNavTop = (i - 1) * 114
            setActiveIndex(i)
            return false
          }
        }
      }
    }
  };

  const onScrollToUpper = () => {};

  const RenderDishes = () => {
    return (
      uesDishesData &&
      uesDishesData.map((v, index) => {
        return (<View key={index} id={'main-' + index}>
          {
            v.map((item, i) => {
              return (
                <View className='dishesItem' key={item.id} >
                  <View className='dishesItem_img'>图片xxx</View>
                  <View className='dishesItem_text'>
                    <Text>{item.name}</Text>
                  </View>
                </View>
              );
            })
          }
        </View>)
      })
    );
  };

  return (
    <View className='MenuClassification'>
      <View className='l_Menu'>
        <ScrollView
          className='scrollview_com'
          scrollY
          scrollWithAnimation
          scrollTop={verticalNavTop}
          style={scrollStyle}
          lowerThreshold={20}
          upperThreshold={20}
          // onScrollToUpper={onScrollToUpper} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
          // onScroll={onScroll}
        >
          {classData.map((v, i) => {
            return (
              <View
                className={
                  activeIndex == i ? 'classItem activeIndex' : 'classItem'
                }
                key={v.id}
                onClick={() => setdishesClick(v, i)}
              >
                {v.name}
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View className='r_dishes'>
        <ScrollView
          className='scrollview_com'
          scrollY
          scrollWithAnimation
          scrollTop={0}
          scrollIntoView={'main-' + mainCur}
          style={scrollStyle}
          lowerThreshold={20}
          upperThreshold={20}
          onScrollToUpper={onScrollToUpper} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
          onScroll={onScroll}
        >
          <RenderDishes></RenderDishes>
        </ScrollView>
      </View>
    </View>
  );
}
