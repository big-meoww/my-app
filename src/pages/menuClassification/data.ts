const classData = [
  {
    name: '特价热销',
    id: 1,
  }, {
    name: '特价锅底',
    id: 2
  }, {
    name: '套餐',
    id: 3
  }, {
    name: '无肉不欢',
    id: 4
  }, {
    name: '多维蔬菜',
    id: 5
  }, {
    name: '酒水',
    id: 6
  }, {
    name: '饮料',
    id: 7
  }
]
const dishesData = [
  [ // 特价热销
    {
      name: '锅底（九块九啦）！！',
      price: '29',
      id: '1-1'
    }, {
      name: '精品肥牛',
      price: '45',
      id: '1-2'
    }, {
      name: '懒羊羊肉片',
      price: '48',
      id: '1-3'
    }, {
      name: '',
      price: '29',
      id: '1-1'

    }, {
      name: '红太狼肉肉',
      price: '45',
      id: '1-2'
    }, {
      name: '慢羊羊',
      price: '48',
      id: '1-3'
    }
  ], [ // 特价锅底
    {
      name: '非常辣红油锅！',
      price: '29',
      id: '2-1'
    }, {
      name: '日式寿喜锅',
      price: '45',
      id: '2-2'
    }, {
      name: '麻辣锅',
      price: '48',
      id: '2-3'
    }, {
      name: '香辣锅',
      price: '29',
      id: '2-1'

    }, {
      name: '酸酸甜甜番茄锅',
      price: '45',
      id: '2-2'
    }, {
      name: '泰式冬阴锅',
      price: '48',
      id: '2-3'
    }
  ], [ // 套餐
    {
      name: '喜羊羊+大脚牛套餐',
      price: '29',
      id: '3-1'
    }, {
      name: '暖羊羊+大脚牛套餐',
      price: '45',
      id: '3-2'
    }, {
      name: '费羊羊+大脚牛套餐',
      price: '48',
      id: '3-3'
    }, {
      name: '美羊羊+大脚牛套餐',
      price: '29',
      id: '3-1'

    }, {
      name: '懒羊羊+大脚牛套餐',
      price: '45',
      id: '3-2'
    }
  ], [ // 无肉不欢
    {
      name: '肌肉',
      price: '29',
      id: '4-1'
    }, {
      name: '牛蛙',
      price: '45',
      id: '4-2'
    }, {
      name: '原切肥牛',
      price: '48',
      id: '4-3'
    }, {
      name: '牛上脑',
      price: '29',
      id: '4-1'

    }, {
      name: '鸭肠',
      price: '45',
      id: '4-2'
    }, {
      name: '猪脑',
      price: '48',
      id: '4-3'
    }
  ], [ // 多维蔬菜
    {
      name: '白菜',
      price: '29',
      id: '5-1'
    }, {
      name: '菠菜',
      price: '45',
      id: '5-2'
    }, {
      name: '老豆皮',
      price: '48',
      id: '5-3'
    }, {
      name: '木耳',
      price: '29',
      id: '5-1'

    }, {
      name: '腐竹',
      price: '45',
      id: '5-2'
    }, {
      name: '莴笋',
      price: '48',
      id: '5-3'
    }
  ], [ // 酒水
    {
      name: '二锅头',
      price: '29',
      id: '6-1'
    }, {
      name: '牛栏山',
      price: '45',
      id: '6-2'
    }, {
      name: '茅台',
      price: '48',
      id: '6-3'
    }
  ], [ // 饮料
    {
      name: '雪碧',
      price: '29',
      id: '7-1'
    }, {
      name: '可乐',
      price: '45',
      id: '7-2'
    }, {
      name: '美年达',
      price: '48',
      id: '7-3'
    }, {
      name: '椰汁',
      price: '29',
      id: '7-1'

    }, {
      name: '王老吉',
      price: '45',
      id: '7-2'
    }, {
      name: '娃哈哈',
      price: '48',
      id: '7-3'
    }
  ]]

export {
  classData,
  dishesData
} 