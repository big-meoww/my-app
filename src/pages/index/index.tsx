import Taro, { useLoad } from '@tarojs/taro';
import { View, Text, Image } from '@tarojs/components';
import Swiper from '@/components/swiper'
import food from '@/assets/img/food.png'
import order from '@/assets/img/order.png'
import vip from '@/assets/img/vip.png'
import nav from '@/assets/img/nav.png'
import zuoJianTou from '@/assets/img/zuoJianTou.png'
import './index.scss';

export default function Index() {
  useLoad(() => {
  });
  const applicationList = [
    { color: '#f8d9d7', src: food, name: '线上点餐' },
    { color: '#fdf8f2', src: order, name: '我的订单' },
    { color: '#f1fee0', src: vip, name: '会员中心' },
    { color: '#dee3f9', src: nav, name: '门店导航' },
  ];
  function go(item) {
    switch (item) {
      case '线上点餐':
        Taro.redirectTo({
          url: `/pages/orderFood/index`
        })
        break;
        // switchTab
      default:
        break;
    }

  }
  return (
    <View className='index'>
      <Swiper />
      <View className='tableNum'>
        <Text>桌号 A01</Text>
        <Text></Text>
        <View className='text'>
          预约
          <Image
            style='width:20px;height:20px;vertical-align: middle;'
            src={zuoJianTou}
          ></Image>
        </View>
      </View>
      <View className='applicationBox'>
        {applicationList &&
          applicationList.map((item) => (
            <View
              className='application_item'
              style={{ background: `${item.color}` }}
              key={item.name}
              onClick={() => {
                go(item.name);
              }}
            >
              <View className='application_item_center'>
                <Image
                  style='width:90%;height:90%;vertical-align: middle;margin-bottom:30px'
                  src={item.src}
                ></Image>
                <Text style=''>{item.name}</Text>
              </View>
            </View>
          ))}
      </View>
    </View>
  );
}
