import { View, Text, Swiper, SwiperItem, Image } from "@tarojs/components";
import { useLoad } from "@tarojs/taro";
import "./index.scss";
import { useState } from "react";

export default function Index() {
  useLoad(() => {
    console.log("Page loaded.");
  });
  const [background, setBackground] = useState([
    {
      class: "demo-text-1",
      src: "https://img0.baidu.com/it/u=523183801,195167993&fm=253&fmt=auto&app=138&f=JPEG?w=750&h=500",
    },
    {
      class: "demo-text-2",
      src: "https://img0.baidu.com/it/u=400488092,4002222001&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500",
    },
    {
      class: "demo-text-3",
      src: "https://img2.baidu.com/it/u=4065039570,3293094685&fm=253&fmt=auto&app=138&f=JPEG?w=750&h=500",
    },
  ]);
  const swiperOption = {
    circular: true,
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
  };
  const cai = [
    {
      img: "https://img10.360buyimg.com/n0/jfs/t1/153371/13/15281/282347/6003f451E313e72cd/c07bc6330fa8b3ab.jpg",
    },
    {
      img: "https://img0.baidu.com/it/u=1878280741,2783514552&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750",
    },
    {
      img: "https://img1.baidu.com/it/u=1239426388,2270166173&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=753",
    },
    {
      img: "http://t15.baidu.com/it/u=669434102,473505117&fm=224&app=112&f=JPEG?w=500&h=500",
    },
    {
      img: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fi2%2F2226815432%2FTB2pi3Ye3jN8KJjSZFkXXaboXXa_%21%212226815432.jpg&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1695452809&t=b2a9d506cf9d672a722e34210f61863f",
    },
    {
      img: "https://img10.360buyimg.com/imgzone/jfs/t1/145837/3/6530/92198/5f43204aE729d4336/ac0e57b5470d933a.jpg",
    },
  ];
  return (
    <View className="index">
      <Swiper
        className="_swiper"
        indicator-dots={swiperOption.indicatorDots}
        autoplay={swiperOption.autoplay}
        interval={swiperOption.interval}
        duration={swiperOption.duration}
        circular={swiperOption.circular}
      >
        {background &&
          background.map((item, index) => (
            <SwiperItem key={index}>
              <view className="swiper-item _swiper">
                <Image
                  src={item.src}
                  style="width: 100%;height: 100%;background: #fff;"
                />
              </view>
            </SwiperItem>
          ))}
      </Swiper>
      <View className="contentBox">
        <Text className="h3">喵喵火锅</Text>
        <View className="h5">MIAOMIAOHUOGUO</View>
        <View style="width:60%;margin: 20px auto;text-indent: 2em;">
          火锅的起源可以追溯到唐朝，当时的中国南方经常发生洪水和涝灾，人们为了防止感冒和疾病，就用锅煮水来喝。后来，他们开始在煮水中加入各种食材，比如肉、蔬菜和豆制品，火锅便渐渐形成。
        </View>
        <View className="caiBox">
          {cai.map((item) => (
            <View className="caiItem">
              <Image src={item.img} mode="scaleToFill"></Image>
            </View>
          ))}
        </View>
        <View className="bottom">
          <Text className="">miaomiao@163.com</Text>
          <View>version：v1.0.0</View>
        </View>
      </View>
    </View>
  );
}
