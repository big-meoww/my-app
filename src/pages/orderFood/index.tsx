import { useState } from "react";
import { View } from "@tarojs/components";
import { useLoad } from "@tarojs/taro";
import Swiper from '@/components/swiper'
import LeftMenuClassification from '../menuClassification/index'
import "./index.scss";

export default function orderFood() {
  console.log("Page83478923749.");
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [dishesData, setDishesData] = useState([])

  return (
    <View className='orderFood'>
      <Swiper />
      <LeftMenuClassification />
    </View>
  );
}
